# thingspeak example

A simple example of sending data to thingspeak from a Raspberry Pi

# Development usage

You need Python above version 3.5, pip installed and a thingspeak.com account + channel 

1. Clone the repository `git clone git@gitlab.com:npes-py-experiments/thingspeak-example.git` 
2. Create a virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment 
2. Activate the virtual environment https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#activating-a-virtual-environment
3. Install requirements `pip install -r requirements.txt`
4. Copy your thingspeak channel id to a file named `channel.txt` , place it in the thingspeak-example folder
5. Copy your thingspeak write api key to a file named `channel.txt`, place it in the thingspeak-example folder   
4. Run `python3 thingspeak_test.py` 