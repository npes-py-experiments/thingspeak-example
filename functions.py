
import sys
# GPIO definitions for RPi
import RPi.GPIO as GPIO
# The time library is used to introduce delays in the code
import time

# open and format write key
def get_key(filename, out=sys.stdout):
    out.write('using key path {}\n'.format(filename))
    with open(filename) as f:
        key = f.readline()
    out.write("- read key with {} chars\n".format(len(key.strip())))
    return key.strip()

# Initialize RPi GPIO pins
def init_gpio():
    GPIO.setwarnings(False)  # Ignore warning for now
    GPIO.setmode(GPIO.BOARD)  # Use physical pin numbering
    return('- gpio setup done')

# Encapsulates the functionality of a button
class Button():

    # Variables
    input_pin = 0
    counter = 0

    # Constructor
    def __init__(self, input_pin):
        self.input_pin = input_pin
        # Set GPIO pin to input, activate pull up
        GPIO.setup(self.input_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        # Attach event and callback function to button
        GPIO.add_event_detect(self.input_pin, GPIO.FALLING, self.event_callback)

    # Callback function, triggered when the button is pushed
    def event_callback(self, channel):
        self.counter += 1
        print(f'Button on pin {self.input_pin} pressed: {self.counter} times')

# Encapsulates the functionality of an LED
class Led():

    # Variables
     output_pin = 0
     blink_rate = 0

    # Constructor
     def __init__(self, output_pin, blink_rate):
         self.output_pin = output_pin
         self.blink_rate = blink_rate
         # Set GPIO pin to output
         GPIO.setup(self.output_pin, GPIO.OUT)
         # Turn off pin
         GPIO.output(self.output_pin, False)

    # Function to flash the Led according to defined blink rate and amount of blink times
     def flash_led(self, blink_times):
        while blink_times > 0:
            GPIO.output(self.output_pin, True)
            time.sleep(self.blink_rate)
            GPIO.output(self.output_pin, False)
            time.sleep(self.blink_rate)
            blink_times -= 1