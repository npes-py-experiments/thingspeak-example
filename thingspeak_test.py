# Using thingspeak library from https://github.com/mchwalisz/thingspeak
import thingspeak

# most of the logic is in the functions.py file
from functions import *

channel_id = get_key('channel.txt')  # PUT YOUR CHANNEL ID IN A FILE CALLED channel.txt LOCATED IN THE SAME FOLDER AS main.py
write_key = get_key('write_key.txt') #PUT YOUR API WRITE KEY IN A FILE CALLED wrirte_key.txt LOCATED IN THE SAME FOLDER AS main.py

# initialize RPi GPIO pins
print(init_gpio())

# attach buttons to pins
button1 = Button(16)
button2 = Button(18)

# attach led's to pins and specify blink rate
status_led1 = Led(22, 0.05)
status_led2 = Led(24, 0.25)

if __name__ == '__main__':

    # initialize the connection to thingspeak
    channel = thingspeak.Channel(id=channel_id, api_key=write_key)

    # Loop forever
    while 1:
        try:
            # update button presses to thingspeak
            response = channel.update({1: button1.counter, 2: button2.counter})
            # Print the response from thingspeak
            print(response)
            # flash led 1, 5 times if response from thingspeak
            status_led1.flash_led(5)
        except:
            # If the connection to thingspeak fails
            print('connection failed')
            # flash led 2 10 times on connection error
            status_led2.flash_led(10)

        # wait 15 seconds because of thingspeak quota limit
        time.sleep(15)

